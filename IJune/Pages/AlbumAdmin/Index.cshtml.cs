using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IJune.Data;
using IJune.Models;

namespace IJune.Pages.AlbumAdmin
{
    public class IndexModel : PageModel
    {
        private readonly IJune.Data.IJuneContext _context;

        public IndexModel(IJune.Data.IJuneContext context)
        {
            _context = context;
        }

        public IList<Album> Album { get;set; }

        public async Task OnGetAsync()
        {
            Album = await _context.Albums.ToListAsync();
        }
    }
}
