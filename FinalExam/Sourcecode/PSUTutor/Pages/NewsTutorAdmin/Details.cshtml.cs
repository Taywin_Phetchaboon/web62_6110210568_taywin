using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSUTutor.Data;
using PSUTutor.Models;

namespace PSUTutor.Pages.NewsTutorAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly PSUTutor.Data.PSUTutorContext _context;

        public DetailsModel(PSUTutor.Data.PSUTutorContext context)
        {
            _context = context;
        }

        public NewsTutor NewsTutor { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsTutor = await _context.NewsTutor.FirstOrDefaultAsync(m => m.NewsTutorID == id);

            if (NewsTutor == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
