using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSUTutor.Data;
using PSUTutor.Models;

namespace PSUTutor.Pages.NewsTutorAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly PSUTutor.Data.PSUTutorContext _context;

        public DeleteModel(PSUTutor.Data.PSUTutorContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewsTutor NewsTutor { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsTutor = await _context.NewsTutor.FirstOrDefaultAsync(m => m.NewsTutorID == id);

            if (NewsTutor == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsTutor = await _context.NewsTutor.FindAsync(id);

            if (NewsTutor != null)
            {
                _context.NewsTutor.Remove(NewsTutor);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
