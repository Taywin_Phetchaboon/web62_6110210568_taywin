using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PSUTutor.Data;
using PSUTutor.Models;

namespace PSUTutor.Pages.NewsTutorAdmin
{
    public class CreateModel : PageModel
    {
        private readonly PSUTutor.Data.PSUTutorContext _context;

        public CreateModel(PSUTutor.Data.PSUTutorContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public NewsTutor NewsTutor { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.NewsTutor.Add(NewsTutor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}