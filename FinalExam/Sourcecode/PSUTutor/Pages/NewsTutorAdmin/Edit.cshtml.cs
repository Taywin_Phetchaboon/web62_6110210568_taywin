using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PSUTutor.Data;
using PSUTutor.Models;

namespace PSUTutor.Pages.NewsTutorAdmin
{
    public class EditModel : PageModel
    {
        private readonly PSUTutor.Data.PSUTutorContext _context;

        public EditModel(PSUTutor.Data.PSUTutorContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewsTutor NewsTutor { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsTutor = await _context.NewsTutor.FirstOrDefaultAsync(m => m.NewsTutorID == id);

            if (NewsTutor == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(NewsTutor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsTutorExists(NewsTutor.NewsTutorID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool NewsTutorExists(int id)
        {
            return _context.NewsTutor.Any(e => e.NewsTutorID == id);
        }
    }
}
