using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSUTutor.Data;
using PSUTutor.Models;

namespace PSUTutor.Pages.NewsTutorAdmin
{
    public class IndexModel : PageModel
    {
        private readonly PSUTutor.Data.PSUTutorContext _context;

        public IndexModel(PSUTutor.Data.PSUTutorContext context)
        {
            _context = context;
        }

        public IList<NewsTutor> NewsTutor { get;set; }

        public async Task OnGetAsync()
        {
            NewsTutor = await _context.NewsTutor.ToListAsync();
        }
    }
}
