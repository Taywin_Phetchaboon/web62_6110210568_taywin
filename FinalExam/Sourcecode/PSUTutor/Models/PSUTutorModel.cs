using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace PSUTutor.Models
{
    public class NewsUser : IdentityUser{ 
        public string FirstName { get; set;} 
        public string LastName { get; set;} 
    }

    public class NewsTutor{
        public int NewsTutorID { get; set;}
        public string TutorName { get; set;}
        public string Subject{ get; set;}
        public string Location { get; set;}
        public int Hour { get; set;}
        public string Cost  { get; set;}

        public string NewsUserId {get; set;}
        public NewsUser postUser {get; set;}
    
    }
}
    
     