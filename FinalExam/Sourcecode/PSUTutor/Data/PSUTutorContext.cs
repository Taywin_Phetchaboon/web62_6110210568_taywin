using PSUTutor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace PSUTutor.Data
{
    public class PSUTutorContext : IdentityDbContext<NewsUser>
    {
       
        public DbSet<NewsTutor> NewsTutor { get; set; }
    
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=PSUTutor.db");
        }
    }
}